<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Image;
use App\Entity\Ingredient;
use App\Entity\Recipe;
use App\Form\CommentType;
use App\Form\RecipeType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RecipeController extends AbstractController
{
    /**
     * @Route("/recipes", name="show_recipes")
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $recipes = $entityManager->getRepository(Recipe::class);

        $recipesQuery = $recipes->createQueryBuilder('r')->getQuery();

        $pagination = $paginator->paginate(
            $recipesQuery,
            $request->query->getInt('page', 1),
            3
        );

        $ingredients = $entityManager->getRepository(Ingredient::class)->findAll();

        $ingredientsForm = $this->createFormBuilder(null)
            ->add('ingredient', EntityType::class, [
                'class' => Ingredient::class,
                'multiple' => true,
            ])
            ->add('submit', SubmitType::class)
            ->getForm();

        $ingredientsForm->handleRequest($request);

        if ($ingredientsForm->isSubmitted() && $ingredientsForm->isValid()) {
            $finalString = '';
            foreach ($ingredientsForm['ingredient']->getData() as $ingredient) {
                $finalString .= $ingredient->getName() . '-';
            }

            $finalString = substr($finalString, 0 ,-1);

            return $this->redirectToRoute('show_filtered_recipes', ['filters' => $finalString]);
        }

        return $this->render('recipe/index.html.twig', [
            'pagination' => $pagination,
            'ingredients' => $ingredients,
            'form' => $ingredientsForm->createView()
        ]);
    }

    /**
     * @Route("/recipes/{filters}", name="show_filtered_recipes")
     */
    public function filteredIndex(Request $request, PaginatorInterface $paginator, string $filters) {
        $entityManager = $this->getDoctrine()->getManager();
        $recipes = $entityManager->getRepository(Recipe::class);
        $ingredients = $entityManager->getRepository(Ingredient::class);

        $finalFilters = explode("-", $filters);
        
        if (count($finalFilters) === 1) {
            $filteredRecipes = $recipes->createQueryBuilder('r')
                ->leftJoin('r.ingredients', 'ingredients')
                ->where('ingredients.name LIKE :name')
                ->setParameter('name', $finalFilters)
            ;
        } else {
            $filteredRecipes = $recipes->createQueryBuilder('r')
                ->leftJoin('r.ingredients', 'ingredients')
            ;
            foreach ($finalFilters as $finalFilter) {
                $filteredRecipes->orWhere('ingredients.name LIKE :name')
                    ->setParameter('name', $finalFilter)
                ;
            }
        }

        $pagination = $paginator->paginate(
            $filteredRecipes,
            $request->query->getInt('page', 1),
            5
        );

        $ingredientsForm = $this->createFormBuilder(null)
            ->add('ingredient', EntityType::class, [
                'class' => Ingredient::class,
                'multiple' => true,
            ])
            ->add('submit', SubmitType::class)
            ->getForm();

        $ingredientsForm->handleRequest($request);

        if ($ingredientsForm->isSubmitted() && $ingredientsForm->isValid()) {
            $finalString = '';
            foreach ($ingredientsForm['ingredient']->getData() as $ingredient) {
                $finalString .= $ingredient->getName() . '-';
            }

            $finalString = substr($finalString, 0 ,-1);

            return $this->redirectToRoute('show_filtered_recipes', ['filters' => $finalString]);
        }

        return $this->render('recipe/index.html.twig', [
            'pagination' => $pagination,
            'ingredients' => $ingredients->findAll(),
            'form' => $ingredientsForm->createView(),
            'filters' => $finalFilters
        ]);
    }

    /**
     * @Route("/recipe/{id}", name="view_recipe", requirements={"id"="\d+"})
     */
    public function showSingleRecipe(Request $request, int $id, PaginatorInterface $paginator)
    {
        $em = $this->getDoctrine()->getManager();
        $recipes = $em->getRepository(Recipe::class);
        $recipeID = $recipes->find($id);
        $comments = $em->getRepository(Comment::class);

        if ($recipeID === null) {return $this->redirectToRoute('show_recipes');}

        $image = $em->getRepository(Image::class)->findOneBy(['recipe' => $recipeID]);
        
        $commentsQuery = $comments->createQueryBuilder('c')
            ->leftJoin('c.recipe', 'recipe')
            ->where('recipe.id = :id')
            ->setParameter('id', $id)
        ;

        $pagination = $paginator->paginate(
            $commentsQuery,
            $request->query->getInt('page', 1),
            5
        );
        $commentForm = $this->createForm(CommentType::class);
        $commentForm->handleRequest($request);
        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $user = $this->getUser();
            $comment = new Comment();
            $comment->setUser($user);
            $comment->setContent($commentForm['content']->getData());
            $comment->setRecipe($recipeID);
            $comment->setDate(new \DateTime('now'));
            $em->persist($comment);
            $em->flush();

            $this->addFlash('notice', 'Your comment has been added!');

            return $this->redirectToRoute('view_recipe', ['id' => $recipeID->getId()]);
        }

        return $this->render('recipe/viewrecipe.html.twig', [
            'pagination' => $pagination,
            'recipe' => $recipeID,
            'commentForm' => $commentForm->createView(),
            'image' => $image
        ]);
    }

    /**
     * @Route("/recipe/add", name="add_recipe")
     */
    public function new(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $ingredient = $em->getRepository(Ingredient::class);

        if ($ingredient->findAll() == null) {
            $this->addFlash('danger', 'No ingredients in the database!');

            return $this->redirectToRoute('home');
        }

        $recipe = new Recipe();

        $user = $this->getUser();

        $form = $this->createForm(RecipeType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $recipe->setName($form['name']->getData());
            $recipe->setMakingTime($form['makingTime']->getData());
            $recipe->setDifficulty($form['difficulty']->getData());
            foreach ($form['ingredients']->getData() as $ingredient) {
                $recipe->addIngredient($ingredient);
            }
            $recipe->setSteps($form['steps']->getData());
            $recipe->setUser($user);
            $recipe->setScore();
            $form['image']->getData() ? $recipe->addImage($form['image']->getData()) : null;
            $em->persist($recipe);
            $em->flush();

            $this->addFlash('notice', 'Your recipe has been added!');

            return $this->redirectToRoute('view_recipe', ['id' => $recipe->getId()]);
        }

        return $this->render('recipe/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/recipe/edit", name="edit_recipe")
     */
    public function edit(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $recipe = $em->getRepository(Recipe::class)->find($id);

        if ($this->getUser() != $recipe->getUser()) {
            return $this->redirectToRoute('home');
        }

        $form = $this->createForm(RecipeType::class, $recipe);

        $form->setData($recipe);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Your recipe has been updated!');

            return $this->redirectToRoute('view_recipe', ['id' => $id]);
        }

        return $this->render('recipe/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/recipe/delete/{id}", name="delete_recipe")
     */
    public function remove(int $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $recipe = $entityManager->getRepository(Recipe::class)->find($id);

        if ($this->getUser() != $recipe->getUser()) {
            return $this->redirectToRoute('home');
        }

        $this->addFlash('success', 'Your recipe, ' . $recipe->getName() . ' has been deleted!');

        $entityManager->remove($recipe);
        $entityManager->flush();

        return $this->redirectToRoute('show_recipes');
    }

    /**
     * @Route("/recipe/like/{id}", name="like_recipe")
     */
    public function like(int $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $recipe = $entityManager->getRepository(Recipe::class)->find($id);

        $user = $this->getUser();

        if ($recipe->getLikes()->contains($user)) {
            $recipe->removeLike($user);
            $this->addFlash('success', 'Disliked!');
        } else {
            $recipe->addLike($user);
            $this->addFlash('success', 'Liked!');
        }

        $recipe->setScore();
        $entityManager->flush();

        return $this->redirectToRoute('view_recipe', ['id' => $id]);
    }
}
