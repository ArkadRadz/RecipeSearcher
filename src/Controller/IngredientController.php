<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Form\IngredientType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IngredientController extends AbstractController
{
    /**
     * @Route("/ingredient", name="ingredient")
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $ingredients = $this->getDoctrine()->getRepository(Ingredient::class);

        $ingredientsQuery = $ingredients->createQueryBuilder('i')->getQuery();

        $pagination = $paginator->paginate(
            $ingredientsQuery,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('ingredient/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/ingredient/add", name="add_ingredient")
     */
    public function add(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $ingredient = new Ingredient();

        $user = $this->getUser();

        $form = $this->createForm(IngredientType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ingredientName = $form['name']->getData();

            if ($em->getRepository(Ingredient::class)->findOneBy(['name' => $ingredientName]))
            {
                $this->addFlash('danger', 'This ingredient already is in the database!');

                return $this->redirectToRoute('add_ingredient');
            }

            $ingredient->setName($ingredientName);
            $ingredient->setUser($user);

            $em->persist($ingredient);
            $em->flush();

            $this->addFlash('success', 'Your ingredient has been added!');

            return $this->redirectToRoute('ingredient');
        }

        return $this->render('new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ingredient/delete/{id}", name="delete_ingredient")
     */
    public function remove(int $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $ingredient = $entityManager->getRepository(Ingredient::class)->find($id);

        if ($this->getUser() != $ingredient->getUser()) {
            return $this->redirectToRoute('home');
        }

        $this->addFlash('success', 'Your ingredient, '.$ingredient->getName().' has been deleted!');

        $entityManager->remove($ingredient);
        $entityManager->flush();

        return $this->redirectToRoute('ingredient');
    }
}
