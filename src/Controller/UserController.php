<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/user/{id}", name="user", requirements={"id"="\d+"})
     */
    public function index(int $id)
    {
        /** @var User $user */
        $user = $this->getUser();

        $userDoctrine = $this->getDoctrine()->getManager()->getRepository(User::class)->find($id);

        return $this->render('user/index.html.twig', [
            'user' => $user,
            'userDoctrine' => $userDoctrine,
        ]);
    }

    /**
     * @Route("/user/edit", name="user_edit")
     */
    public function edit(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createForm(RegistrationFormType::class, $user);

        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Your profile has been updated!');

            return $this->redirectToRoute('user', ['id' => $user->getId()]);
        }

        return $this->render('user/update.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
