<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $strongPassword = true;

            if (!preg_match('/[A-Z]/', $form->get('plainPassword')->getData())) {
                    $strongPassword = false;
                    $this->addFlash('danger', 'Please use an uppercase letter in the password');
            }

            if (!preg_match('/[a-z]/', $form->get('plainPassword')->getData())) {
                $strongPassword = false;
                $this->addFlash('danger', 'Please use a lowercase letter in the password');
            }

            if (!preg_match('/[0-9]/', $form->get('plainPassword')->getData())) {
                $strongPassword = false;
                $this->addFlash('danger', 'Please use a number in the password');
            }

            if ($strongPassword === false) {
                return $this->redirectToRoute('app_register');
            }

            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email

            return $this->redirectToRoute('home');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
