<?php

namespace App\Controller;

use App\Entity\Image;
use App\Form\ImageType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ImageController extends AbstractController
{
    /**
     * @Route("/images", name="images")
     */
    public function index()
    {
        $images = $this->getDoctrine()->getRepository(Image::class)->findAll();

        return $this->render('image/index.html.twig', [
            'images' => $images
        ]);
    }

    /**
     * @Route("image/add", name="add_image")
     */
    public function new(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(ImageType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Image $image */
            $image = $form->getData();
            $imageMimeType = $image->getImageFile()->getMimeType();

            if (! in_array($imageMimeType, ['image/jpeg', 'image/png'])) {
                $this->addFlash('danger', 'Not a valid image type!');
                
                return $this->redirectToRoute('add_image');
            }

            $image->setUser($this->getUser());
            $em->persist($image);
            $em->flush();

            $this->addFlash('success', 'Image added.');

            return $this->redirectToRoute('images');
        }

        return $this->render('image/new.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
